# Base CRM API client
[ ![Codeship Status for SethCalkins/basecrm-1](https://codeship.io/projects/0bdff4e0-31b0-0132-3617-42b9a23cf717/status)](https://codeship.io/projects/40159)
[![Build Status](https://travis-ci.org/basecrm/basecrm.png?branch=master)](https://travis-ci.org/basecrm/basecrm)

New and shiny client for the Base CRM API

## Installation

The gem available via Rubygems. To install it, use the following command:

```ruby
sudo gem install basecrm
```

If you use Bundler, put this in your Gemfile:

```ruby
gem 'basecrm'
```

To get the latest version, put this in your Gemfile:

```ruby
gem 'basecrm', :git => 'git://github.com/basecrm/basecrm.git'
```

## Usage

Before using this gem you must require it by calling:

```ruby
require 'basecrm'
```

## Usage

For information on getting started, see our [wiki page](https://github.com/basecrm/basecrm/wiki).

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request